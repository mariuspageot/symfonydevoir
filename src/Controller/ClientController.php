<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\ClientType;
use App\Form\SupprimerType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ClientController extends AbstractController
{
    /**
     * @Route("/client", name="client")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Client::class);

        $clients = $repository->findAll();

        return $this->render('client/index.html.twig', [
            'clients' => $clients,
        ]);
    }
    /**
     * @Route("/client/ajouter", name="ajouter_client")
     */
    public function ajouter(Request $request)
    {
        $client = new Client();

        //creation du formulaire
        $formulaire = $this->createForm(ClientType::class, $client);

        $formulaire->handleRequest($request);

        if ($formulaire->isSubmitted() && $formulaire->isValid()) {

            //recuperer l'entity manager (sorte de connexion a la bdd)
            $em = $this->getDoctrine()->getManager();

            //je dis au manager que je veux ajouter la categorie dans la bdd
            $em->persist($client);

            $em->flush();

            return $this->redirectToRoute("client");
        }

        return $this->render('client/formulaire.html.twig', [
            'formulaire' => $formulaire->createView(),
            'h1' => 'ajouter un client',
        ]);
    }
    /**
     * @Route("/client/modifier/{id}", name="modifier_client")
     */
    public function modifier($id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Client::class);
        $client = $repository->find($id);

        //creation du formulaire
        $formulaire = $this->createForm(ClientType::class, $client);

        $formulaire->handleRequest($request);

        if ($formulaire->isSubmitted() && $formulaire->isValid()) {

            //recuperer l'entity manager (sorte de connexion a la bdd)
            $em = $this->getDoctrine()->getManager();

            //je dis au manager que je veux ajouter la categorie dans la bdd
            $em->persist($client);

            $em->flush();

            return $this->redirectToRoute("client");
        }

        return $this->render('client/formulaire.html.twig', [
            'formulaire' => $formulaire->createView(),
            'h1' => 'modification <i>' . $client->getNom() . '</i>',
        ]);
    }
    /**
     * @Route("/client/supprimer/{id}", name="supprimer_client")
     */
    public function supprimer($id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Client::class);
        $client = $repository->find($id);

        //creation du formulaire
        $formulaire = $this->createForm(SupprimerType::class, $client);

        $formulaire->handleRequest($request);

        if ($formulaire->isSubmitted() && $formulaire->isValid()) {

            //recuperer l'entity manager (sorte de connexion a la bdd)
            $em = $this->getDoctrine()->getManager();

            //je dis au manager que je veux ajouter la categorie dans la bdd
            $em->remove($client);

            $em->flush();

            return $this->redirectToRoute("client");
        }

        return $this->render('client/formulaireSuppr.html.twig', [
            'formulaire_suppression' => $formulaire->createView(),
            'h1' => 'Supprimer un client',
        ]);
    }
}
